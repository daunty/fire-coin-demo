package com.maizi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
//		System.setProperty("https.protocols", "TLSv1.2,TLSv1.1,SSLv3");
		SpringApplication.run(DemoApplication.class, args);
	}
}
