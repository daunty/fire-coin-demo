package com.maizi.entity;

import lombok.Data;

@Data
public class HuobiResponse<T> {

	public String status;

	public String errCode;

	public String errMsg;

	private String ch;

	private String ts;

	public T tick;

}
