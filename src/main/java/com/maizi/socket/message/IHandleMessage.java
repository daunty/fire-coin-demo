package com.maizi.socket.message;

import java.util.List;

import org.springframework.web.socket.TextMessage;

public interface IHandleMessage {

	/**
	 * 需要订阅的目标
	 */
	List<TextMessage> subscribes();

	/**
	 * 订阅成功消息处理
	 */
	void subscribeCallback(String message);

	/**
	 * 服务器返回数据回调处理
	 */
	void call(String message);

	/**
	 * webSocket连接异常处理
	 */
	void webSocketError(Throwable exception);

	/**
	 * connection连接关闭通知
	 */
	void afterConnectionClosed();

}
